package validator

import (
	"encoding/json"
	"github.com/go-playground/validator/v10"
	"fmt"
)

//Location - struct
type Location struct {
	Uid string `validate:"required"`
	Evse_id string `validate:"required,max=48"`
}

//IsValid - validate location json file
func IsValid(jsonData string) bool{

	   var v Location
	   json.Unmarshal([]byte(jsonData), &v)
	   validate := validator.New()
	   err := validate.Struct(&v)
	   fmt.Println(err)
	   if err != nil {
		   return false
	   }

	   return true
}


