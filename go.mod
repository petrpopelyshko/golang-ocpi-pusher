module github.com/neok/ocpi

go 1.13

require (
	github.com/confluentinc/confluent-kafka-go v1.3.0
	github.com/go-playground/validator/v10 v10.2.0
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.3
)
