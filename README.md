# OCPI pusher



# WITH Docker compose (Docker compose not working atm)

`docker-compose build && docker-compose up -d`

ISSUES WITH DOCKER COMPOSE (KAFKA image)

https://github.com/wurstmeister/kafka-docker/issues/211

# LOCALLY 

## Requirements

- $ brew cask install java
- $ brew install kafka
- vim /usr/local/etc/kafka/server.properties
- listeners=PLAINTEXT://:9092
- zookeeper-server-start /usr/local/etc/kafka/zookeeper.properties
- kafka-server-start /usr/local/etc/kafka/server.properties


## How to install

- docker build -t ocpi
- docker run -d -p 3001:3001 ocpi

![](img/img.png)


