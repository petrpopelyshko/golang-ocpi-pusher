package producer

import (
	"fmt"
	"github.com/confluentinc/confluent-kafka-go/kafka"
	"log"
	"os"
)

var kafkaServer, kafkaTopic string

func init() {
	kafkaServer = getEnv("KAFKA_SERVER", "localhost:9092")
	kafkaTopic = getEnv("KAFKA_TOPIC", "test")
}

// SendMessage send message to Kafka
func SendMessage(message string) {

	p, err := kafka.NewProducer(&kafka.ConfigMap{"bootstrap.servers": kafkaServer})
	if err != nil {
		log.Println(fmt.Sprintf("Failed to create producer: %s\n", err))
		os.Exit(1)
	}
	log.Println(fmt.Sprintf("Sending message %s\n", message))
	deliveryChan := make(chan kafka.Event)

	err = p.Produce(&kafka.Message{
		TopicPartition: kafka.TopicPartition{Topic: &kafkaTopic, Partition: kafka.PartitionAny},
		Value:          []byte(message),
	}, deliveryChan)

	e := <-deliveryChan
	m := e.(*kafka.Message)

	if m.TopicPartition.Error != nil {
		log.Println(fmt.Sprintf("Delivery failed: %v\n", m.TopicPartition.Error))
	} else {
		log.Println(fmt.Sprintf("Delivered message to topic %s [%d] at offset %v\n",
			*m.TopicPartition.Topic, m.TopicPartition.Partition, m.TopicPartition.Offset))
	}

	close(deliveryChan)
}


func getEnv(key string, defaultVal string) string {
    if value, exists := os.LookupEnv(key); exists {
	return value
    }

    return defaultVal
}