package main

import (
	"os"
	"net/http"
	"github.com/gorilla/mux"
	"github.com/gorilla/handlers"
	"github.com/neok/ocpi/producer"
	"github.com/neok/ocpi/validator"
	"io/ioutil"
	"log"
	"strings"
)

func main() {
	router := mux.NewRouter();

	router.HandleFunc("/locations/{countryCode}/{isoId}/{locationId}", LocationHandler)
	amw := getMiddleware();

	router.Use(amw.Middleware);
	router.Use(amw.ValidationMiddleware);
	http.ListenAndServe(":3001", handlers.LoggingHandler(os.Stdout, router))
}
//LocationHandler info
func LocationHandler(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
    if err != nil {
        panic(err)
    }
	producer.SendMessage(string(body))

	w.WriteHeader(204)
}

type authMiddleware struct {
	token string
}

func getMiddleware() authMiddleware {
	amw := authMiddleware{}
	amw.token = "afksdhfasjdf2374179==00"

	return amw
}


func (amw *authMiddleware) ValidationMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			panic(err)
		}

		if !validator.IsValid(string(body)) {
			http.Error(w, "Invalid json provided", http.StatusBadRequest)
		}
	})
}


func (amw *authMiddleware) Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Println(r.Header.Get("Authorization"))
		slice := strings.SplitN(r.Header.Get("Authorization"), " ", 2)

		if len(slice) != 2 || slice[0] != "Token" {
			http.Error(w, "Unauthorized", http.StatusUnauthorized)
			
			return
		}

		if slice[1] == amw.token {
			log.Println("Authenticated user")
			next.ServeHTTP(w, r)
		} else {
			http.Error(w, "Unauthorized", http.StatusUnauthorized)
		}
	})
}
